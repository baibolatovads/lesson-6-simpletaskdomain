package domain

type Todo struct {
	UserId    int		`json:"userId"`
	id        int		`json:"id"`
	Title     string	`json:"title"`
	Completed bool		`json:"completed"`
}
