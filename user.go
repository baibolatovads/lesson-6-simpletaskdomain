package domain

type User struct {
	UserID int
	Posts  []Post
	Albums []Album
	Todos  []Todo
}
