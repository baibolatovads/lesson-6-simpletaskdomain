package domain

type Post struct {
	UserID   int    `json:"userid"`
	Id       int    `json:"id"`
	Title    string `json:"title"`
	Body     string `json:"body"`
	Comments []Comment
}
